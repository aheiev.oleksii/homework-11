let hero = ["Ivan"];
let native = ["York", "Of"];
let destination = ["Poltava", "In"];

let rainbow = [].concat(destination, native, hero);
rainbow.reverse();

rainbow.shift();
rainbow.unshift("Richard");
let endOfArr = rainbow.splice(3, 2);
rainbow.push("Gave", "Battle");
rainbow = rainbow.concat(endOfArr);
rainbow.pop();
rainbow.push("Vain");

let colors = ["red", "orange", "yellow", "green", "skyblue", "blue", "purple"];
let circleArr = [];

for (let i = 0; i < rainbow.length; i++) {
  circleArr.push(`
  <div class="element">
    <div class="circle" style="background-color: ${colors[i]}"></div>
    <p style="margin-top: 50px">${rainbow[i]}</p>
  </div>
    `);
}

document.write(`
    <div class="wrapper">
        ${circleArr.join("")}
    </div>
`);
